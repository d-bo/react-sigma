import {ADD_TOKEN, ADD_CHANNEL} from '../constants/action-types';
import {DELETE_TOKEN, DELETE_CHANNEL} from '../constants/action-types';
import {EDIT_TOKEN, EDIT_CHANNEL} from '../constants/action-types';

export const addToken = token => ({
	type: ADD_TOKEN,
	payload: token
});

export const deleteToken = token => ({
	type: DELETE_TOKEN,
	payload: token
});

export const editToken = token => ({
	type: EDIT_TOKEN,
	payload: token
});

export const addChannel = channel => ({
	type: ADD_CHANNEL,
	payload: channel
});

export const deleteChannel = channel => ({
	type: DELETE_CHANNEL,
	payload: channel
});

export const editChannel = channel => ({
	type: EDIT_CHANNEL,
	payload: channel
});