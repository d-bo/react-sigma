import {ADD_TOKEN, ADD_CHANNEL} from '../constants/action-types';
import {DELETE_TOKEN, DELETE_CHANNEL} from '../constants/action-types';
import {EDIT_TOKEN, EDIT_CHANNEL} from '../constants/action-types';

var telegramBotTokens = [], telegramChannels = [];

// Get tokens, channels from 
const tokens = localStorage.getItem('tokens');
if (tokens) {
    telegramBotTokens = JSON.parse(tokens);
}
const channels = localStorage.getItem('channels');
if (channels) {
    telegramChannels = JSON.parse(channels);
}

const initialState = {
	telegramBotTokens,
	telegramChannels,
};

const rootReducer = ( state = initialState, action ) => {
	var newState;
	switch (action.type) {
		case ADD_TOKEN:
            localStorage.setItem('tokens', 
            	JSON.stringify([...state.telegramBotTokens, action.payload]));
			return {...state, telegramBotTokens: [...state.telegramBotTokens, action.payload]};
		case ADD_CHANNEL:
            localStorage.setItem('channels', 
            	JSON.stringify([...state.telegramChannels, action.payload]));
			return {...state, telegramChannels: [...state.telegramChannels, action.payload]};
		case EDIT_TOKEN:
			newState = state.telegramBotTokens.filter(el => {
				return el.value !== action.payload.prev;
			});
			localStorage.setItem('tokens', JSON.stringify([...newState, action.payload]));
			return {...state, telegramBotTokens: [...newState, action.payload]};
		case EDIT_CHANNEL:
			newState = state.telegramChannels.filter(el => {
				return el.value !== action.payload.prev;
			});
			console.log(newState);
			localStorage.setItem('channels', JSON.stringify([...newState, action.payload]));
			return {...state, telegramChannels: [...newState, action.payload]};
		case DELETE_TOKEN:
            newState = [];
            console.log(action);
            for (let k of state.telegramBotTokens) {
            	console.log(k.value);
                if (action.payload !== k.value)
                    newState.push(k);
            }
            localStorage.setItem('tokens', JSON.stringify(newState));
			return {...state, telegramBotTokens: newState};
		case DELETE_CHANNEL:
            newState = [];
            for (let k of state.telegramChannels) {
                if (action.payload !== k.value)
                    newState.push(k);
            }
            localStorage.setItem('channels', JSON.stringify(newState));
			return {...state, telegramChannels: newState};
		default:
			return state;
	}
};

export default rootReducer;