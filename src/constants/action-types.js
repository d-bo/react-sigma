export const ADD_ARTICLE = "ADD_ARTICLE";
export const ADD_TOKEN = "ADD_TOKEN";
export const EDIT_TOKEN = "EDIT_TOKEN";
export const EDIT_CHANNEL = "EDIT_CHANNEL";
export const DELETE_TOKEN = "DELETE_TOKEN";
export const ADD_CHANNEL = "ADD_CHANNEL";
export const DELETE_CHANNEL = "DELETE_CHANNEL";