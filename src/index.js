import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import App from './App';
//import registerServiceWorker from './registerServiceWorker';
import { HashRouter } from 'react-router-dom'
import ScrollToTop from './ScrollToTop';
import store from './store/index';
import { Provider } from 'react-redux';

ReactDOM.render(
    <HashRouter>
        <ScrollToTop>
	        <Provider store={store}>
	            <App></App>
	        </Provider>
        </ScrollToTop>
    </HashRouter>,
    document.getElementById('root')
);

//registerServiceWorker();