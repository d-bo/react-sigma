import React, {Component} from 'react';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {Dropdown} from 'primereact/dropdown';
import {Growl} from 'primereact/growl';
import {Message} from 'primereact/message';
import {TabView, TabPanel} from 'primereact/tabview';
import {Checkbox} from 'primereact/checkbox';
import {InputTextarea} from 'primereact/inputtextarea';
import {ProgressBar} from 'primereact/progressbar';
import {RadioButton} from 'primereact/radiobutton';
import {Accordion, AccordionTab} from 'primereact/accordion';

import { connect } from 'react-redux';

import { addToken, addChannel, deleteToken, deleteChannel, editToken, editChannel } from '../actions/index';

const mapStateToProps = (state) => {
    return {
        telegramBotTokens: state.telegramBotTokens,
        telegramChannels: state.telegramChannels
    }
};

const mapDispatchToProps = {
    addToken,
    addChannel,
    deleteToken,
    deleteChannel,
    editToken,
    editChannel
};

class TelegramComponent extends Component {

    constructor() {
        super();
        this.state = {

            msgText: '',
            sendMsgDisabled: null,
            radioValue: '',

            file: '',
            imgPreview: '',
            imageFromUrl: '',
            videoFromUrl: '',
            videoFromInputUrl: '',
            replyMarkup: '',

            videoLoading: false,

            inputBotName: '',
            inputBotToken: '',
            inputChannelName: '',
            inputChannelDesc: '',

            inputBotNameError: null,
            inputChannelNameError: null,
            inputBotTokenError: null,
            inputChannelDescError: null,

            dropdownToken: null,
            dropdownDeleteToken: null,
            dropdownChannel: null,
            dropdownDeleteChannel: null,

            checkVideoNote: false,
            checkDeleteToken: false,
            checkDeleteChannel: false,
            deleteButtonDisabled: true,
            deleteButtonChannelDisabled: true,
        };

        this.handleAddBotToken = this.handleAddBotToken.bind(this);
        this.handleAddChannel = this.handleAddChannel.bind(this);
        this.handleDeleteChannel = this.handleDeleteChannel.bind(this);
        this.handleDeleteBotToken = this.handleDeleteBotToken.bind(this);
        this.handleCheckDeleteBotToken = this.handleCheckDeleteBotToken.bind(this);
        this.handleCheckDeleteChannel = this.handleCheckDeleteChannel.bind(this);
        this.selectDropdownChannel = this.selectDropdownChannel.bind(this);
        this.selectDropdownToken = this.selectDropdownToken.bind(this);
        this.handleSendMessage = this.handleSendMessage.bind(this);
        this.getTokenHash = this.getTokenHash.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleVideoChange = this.handleVideoChange.bind(this);
        this.onCheckVideoNoteChange = this.onCheckVideoNoteChange.bind(this);
    }

    onCheckVideoNoteChange() {
        this.setState({checkVideoNote: this.state.checkVideoNote === false ? true : false});
    }

    getTokenHash() {
        var token = this.props.telegramBotTokens.filter(el => {
            return el.value === this.state.dropdownToken;
        });
        return token.length === 1 ? token = token[0].token : false;
    }

    handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
          this.setState({
            file,
            imagePreviewUrl: reader.result
          });
        }

        reader.readAsDataURL(file);
        console.log(file);
    }

    handleVideoChange(e) {
        e.preventDefault();

        this.setState({
            videoLoading: true
        });
        let reader = new FileReader();
        let file = e.target.files[0];

        if (file.size > 50000000) {
            this.growl.show({
                severity: 'success',
                summary: 'File size',
                detail: 'Maximum file size reached - '+file.size
            });
            return false;
        }

        reader.onloadend = () => {
          this.setState({
            file,
            videoFromUrl: reader.result,
            videoLoading: false
          });
        }

        reader.readAsDataURL(file);
    }

    handleSendMessage() {

        if (!this.state.dropdownChannel || !this.state.dropdownToken) {
            this.growl.show({
                severity: 'error',
                summary: 'Error',
                detail: 'Bot and channel selection required'
            });
            return;
        }

        var self = this;
        var apiMethod;

        // testing inline buttons
        var keyboard = {
            "inline_keyboard": [
                [
                    {"text": "Yes", "url": "http://www.google.com/"},
                    {"text": "No", "url": "http://www.google.com/"}
                ]
            ]
        };

        this.setState({sendMsgDisabled: true});
        var formData = new FormData();

        // global CHAT_ID
        formData.append('chat_id', '@'+this.state.dropdownChannel);

        // parse_mode
        if (this.state.radioValue !== '') {
            formData.append('parse_mode', this.state.radioValue);
        }

        // only text
        if (this.state.imagePreviewUrl === '') {
            apiMethod = 'sendMessage';
            formData.append('text', this.state.msgText);
        }

        // image + caption
        if (this.state.file.type === 'image/png' || this.state.file.type === 'image/jpeg') {
            apiMethod = 'sendPhoto';
            if (this.state.msgText !== '')
                formData.append('caption', this.state.msgText);
            formData.append('photo', this.state.file);
        }

        if (this.state.file.type === 'audio/mp3') {
            apiMethod = 'sendAudio';
            formData.append('audio', this.state.file);
            if (this.state.msgText !== '')
                formData.append('caption', this.state.msgText);
        }

        // image from URL
        if (this.state.imagePreviewUrl !== '' && this.state.file === '') {
            apiMethod = 'sendPhoto';
            console.log("URL photo");
            if (this.state.msgText !== '')
                formData.append('caption', this.state.msgText);
            formData.append('photo', this.state.imagePreviewUrl);
        }

        // VIDEO
        if (this.state.videoFromUrl !== '' || this.state.videoFromInputUrl !== '') {
            apiMethod = 'sendVideo';
            var video_source = this.state.videoFromUrl !== '' ? this.state.videoFromUrl : this.state.videoFromInputUrl;
            console.log("URL video");
            if (this.state.msgText !== '')
                formData.append('caption', this.state.msgText);
            formData.append('video', video_source);
            formData.delete('photo');

            // VIDEO NOTE
            if (this.state.checkVideoNote) {
                apiMethod = 'sendVideoNote';
                console.log("URL sendVideoNote");
                if (this.state.msgText !== '')
                    formData.append('caption', this.state.msgText);
                formData.append('video_note', video_source);
                formData.delete('photo');
            }
        }

        if (this.state.replyMarkup !== '')
            formData.append('reply_markup', JSON.stringify(keyboard));

        if (this.state.imagePreviewUrl !== '') {
            fetch(`https://api.telegram.org/bot${this.getTokenHash(this.state.dropdownToken)}/${apiMethod}`, {
                method: 'POST',
                body: formData
            })
            .then(r => r.json())
            .then(data => {
                this.setState({sendMsgDisabled: null});
                if (data.hasOwnProperty('ok')) {
                    if (data.ok === true) {
                        this.growl.show({
                            severity: 'success',
                            summary: 'Send message',
                            detail: 'Success'
                        });
                    }
                }
            }).catch(function() {
                self.setState({sendMsgDisabled: null});
                self.growl.show({
                    severity: 'error',
                    summary: 'Error',
                    detail: 'Something happened'
                });
            });
        }
    }

    handleAddBotToken(event) {

        if (!this.state.inputBotName)
            this.setState({inputBotNameError: true});
        else
            this.setState({inputBotNameError: null});

        if (!this.state.inputBotToken)
            this.setState({inputBotTokenError: true});
        else
            this.setState({inputBotTokenError: null});

        if (this.state.inputBotName && this.state.inputBotToken) {
            // Check if allready exist
            if (this.state.inputBotName !== '' && !this.state.dropdownDeleteToken) {
                for (let k of this.props.telegramBotTokens) {
                    if (k.value === this.state.inputBotName) {
                        this.growl.show({
                            severity: 'warn',
                            summary: this.state.inputBotName,
                            detail: 'Allready exist. Choose another name.'
                        });
                        return;
                    }
                }
            }
            this.setState({
                inputBotName: '',
                inputBotToken: '',
                dropdownDeleteToken: null
            });
            this.growl.show(
                {severity: 'success', summary: this.state.inputBotName, detail: 'Saved'}
            );

            // Edit token
            if (this.state.inputBotName !== '' && this.state.dropdownDeleteToken) {
                this.props.editToken({
                    label: '@'+this.state.inputBotName.replace('@', '')+' - '+this.state.inputBotToken,
                    value: this.state.inputBotName.replace('@', ''),
                    token: this.state.inputBotToken,
                    prev: this.state.dropdownDeleteToken
                });
            }

            // Add new token
            if (this.state.inputBotName !== '' && !this.state.dropdownDeleteToken) {
                this.props.addToken({
                    label: this.state.inputBotName.replace('@', '')+' - '+this.state.inputBotToken,
                    value: this.state.inputBotName.replace('@', ''),
                    token: this.state.inputBotToken
                });
            }
        }
    }

    handleAddChannel() {

        if (!this.state.inputChannelName)
            this.setState({inputChannelNameError: true});
        else
            this.setState({inputChannelNameError: null});

        if (!this.state.inputChannelDesc)
            this.setState({inputChannelDescError: true});
        else
            this.setState({inputChannelDescError: null});

        if (this.state.inputChannelName && this.state.inputChannelDesc) {
            // Check if allready exist
            if (this.state.inputChannelName !== '' && !this.state.dropdownDeleteChannel) {
                for (let k of this.props.telegramChannels) {
                    if (k.value === this.state.inputChannelName) {
                        this.growl.show({
                            severity: 'warn',
                            summary: this.state.inputChannelName,
                            detail: 'Allready exist. Choose another name.'
                        });
                        return;
                    }
                }
            }
            this.setState({
                inputChannelName: '',
                inputChannelDesc: '',
                dropdownDeleteChannel: null
            });
            this.growl.show(
                {severity: 'success', summary: this.state.inputChannelName, detail: 'Saved'
            });

            // Edit token
            if (this.state.inputChannelName !== '' && this.state.dropdownDeleteChannel) {
                this.props.editChannel({
                    label: '@'+this.state.inputChannelName.replace('@', '')+' - '+this.state.inputChannelDesc,
                    value: this.state.inputChannelName.replace('@', ''),
                    desc: this.state.inputChannelDesc,
                    prev: this.state.dropdownDeleteChannel
                });
            }

            // Add new token
            if (this.state.inputChannelName !== '' && !this.state.dropdownDeleteChannel) {
                this.props.addChannel({
                    label: this.state.inputChannelName.replace('@', '')+' - '+this.state.inputChannelDesc,
                    value: this.state.inputChannelName.replace('@', ''),
                    desc: this.state.inputChannelDesc
                });
            }
        }
    }

    handleDeleteBotToken() {
        if (this.state.dropdownDeleteToken) {
            this.props.deleteToken(this.state.dropdownDeleteToken);
            this.setState({checkDeleteToken: false, 
                deleteButtonDisabled: true, dropdownDeleteToken: null, 
                inputBotToken: '', inputBotName: ''});
            this.growl.show({
                severity: 'success', summary: 'Token', detail: 'Deleted'
            });
        } else {
            this.growl.show({
                severity: 'warn', summary: 'Token', detail: 'Select token to delete'
            });
        }
    }

    handleDeleteChannel() {
        if (this.state.dropdownDeleteChannel) {
            this.props.deleteChannel(this.state.dropdownDeleteChannel);
            this.setState({checkDeleteChannel: false, 
                deleteButtonChannelDisabled: true, dropdownDeleteChannel: null, 
                inputChannelDesc: '', inputChannelName: ''});
            this.growl.show({
                severity: 'success', summary: 'Token', detail: 'Deleted'
            });
        } else {
            this.growl.show({
                severity: 'warn', summary: 'Channel', detail: 'Select channel to delete'
            });
        }
    }

    handleCheckDeleteBotToken() {
        if (!this.state.checkDeleteToken) {
            this.setState({checkDeleteToken: true, deleteButtonDisabled: false});
        } else {
            this.setState({checkDeleteToken: false, deleteButtonDisabled: true});
        }
    }

    handleCheckDeleteChannel() {
        if (!this.state.checkDeleteChannel) {
            this.setState({checkDeleteChannel: true, deleteButtonChannelDisabled: false});
        } else {
            this.setState({checkDeleteChannel: false, deleteButtonChannelDisabled: true});
        }
    }

    selectDropdownToken(event) {
        let tokens = this.props.telegramBotTokens.filter(el => {
            return el.value === event.target.value;
        });
        if (tokens.length === 1) {
            this.setState({inputBotName: tokens[0].value, inputBotToken: tokens[0].token});
        }
        this.setState({
            dropdownDeleteToken: event.value, 
            inputBotNameError: null, 
            inputBotTokenError: null
        });
    }

    selectDropdownChannel(event) {
        var channels = this.props.telegramChannels.filter(el => {
            return el.value === event.target.value;
        });
        if (channels.length === 1) {
            this.setState({inputChannelName: channels[0].value, inputChannelDesc: channels[0].desc});
        }
        this.setState({
            dropdownDeleteChannel: event.value,
            inputBotNameError: null,
            inputBotTokenError: null
        });
    }

    render() {
        return (
            <div className="p-grid">
                <div className="p-col-12 p-lg-6">
                    <div className="card card-w-title">
                        <h1>Create message</h1>
                        <p>
                            <Accordion>
                                <AccordionTab header="Message, caption">
                                    <p>
                                        <InputTextarea placeholder="Message text (or image caption)" 
                                        style={{width:'100%'}} rows={4}
                                        value={this.state.msgText} autoResize={true}
                                        onChange={event => this.setState({msgText: event.target.value})} />
                                    </p>
                                    <p style={{textAlign: 'right'}}>
                                            <RadioButton value="Markdown" inputId="rb1"
                                            onChange={event => this.setState({radioValue: event.value})} 
                                            checked={this.state.radioValue === "Markdown"}/>
                                            <label htmlFor="rb1" className="p-radiobutton-label" style={{marginRight: '17px'}}>Markdown</label>
                                            <RadioButton value="HTML" inputId="rb2" onChange={event => this.setState({radioValue: event.value})} checked={this.state.radioValue === "HTML"}/>
                                            <label htmlFor="rb2" className="p-radiobutton-label">HTML</label>
                                    </p>
                                </AccordionTab>

                                <AccordionTab header="Image">
                                    <div className="p-toolbar-group-left">
                                        <Button label="Add image file" icon="pi pi-plus" className="p-button-secondary"
                                            style={{marginBottom:'10px'}} onClick={(e) => document.getElementById('imageUpload').click()} />
                                        <div style={{display: 'none'}}>
                                            <input id="imageUpload" className="fileInput" type="file" 
                                                onChange={(e)=>this.handleImageChange(e)} />
                                        </div>
                                    </div>
                                    <div className="p-toolbar-group-right">
                                        <Button icon="pi pi-times" className="p-button-secondary" label="Clear"
                                        onClick={(e) => {this.setState({file: '', imagePreviewUrl: '', imageFromUrl: ''});} } />
                                    </div>
                                    <div style={{clear: 'both'}}>
                                        <p>
                                            Or paste image from URL:
                                        </p>
                                    </div>
                                    <div>
                                        <InputTextarea rows={4}
                                        placeholder="https://cdn.sstatic.net/Img/unified/sprites.svg?v=e5e58ae7df45" 
                                        style={{width: '100%'}} value={this.state.imageFromUrl}
                                        autoResize={true}
                                        onChange={(e) => {this.setState({imagePreviewUrl: e.target.value, imageFromUrl: e.target.value});}} />
                                    </div>
                                    {this.state.imagePreviewUrl &&
                                        <img src={this.state.imagePreviewUrl} alt='' style={{width: '100%'}} />}
                                </AccordionTab>

                                <AccordionTab header="Video">
                                    <div style={{marginBottom: '10px'}}>
                                        <Checkbox value="video_note" inputId="video_note" 
                                        onChange={this.onCheckVideoNoteChange} 
                                        checked={this.state.checkVideoNote} />
                                        <label htmlFor="video_note" 
                                        className="p-checkbox-label">Send as Video Note (rounded)</label>
                                    </div>
                                    <div className="p-toolbar-group-left">
                                        <Button label="Add video file" icon="pi pi-plus" className="p-button-secondary"
                                            style={{marginBottom:'10px'}} onClick={(e) => document.getElementById('videoUpload').click()} />
                                        <div style={{display: 'none'}}>
                                            <input id="videoUpload" className="fileInput" type="file" 
                                                onChange={(e)=>this.handleVideoChange(e)} />
                                        </div>
                                    </div>
                                    {this.state.videoLoading &&
                                    <div style={{clear: 'both'}}>
                                    <ProgressBar mode="indeterminate" style={{height: '6px'}}></ProgressBar>
                                    </div>
                                    }
                                    <div className="p-toolbar-group-right">
                                        <Button icon="pi pi-times" className="p-button-secondary" 
                                        onClick={(e) => {this.setState({videoFromInputUrl: '', videoFromUrl: ''});} } />
                                    </div>
                                    <div style={{clear: 'both'}}>
                                        <p>
                                            Or paste video from URL:
                                        </p>
                                    </div>
                                    <div style={{clear: 'both'}}>
                                        <InputTextarea rows={4} style={{width: '100%'}} placeholder="Paste video URL" 
                                        value={this.state.videoFrom} 
                                        onChange={(e) => {this.setState({videoFromUrl: e.target.value});}} />
                                    </div>
                                    {this.state.videoFromUrl &&
                                        <div>
                                            <video width={'100%'} controls>
                                                <source src={this.state.videoFromUrl} type="video/mp4" />
                                            </video>
                                        </div>}
                                    {this.state.videoFromInputUrl &&
                                        <div>
                                            <video width={'100%'} controls>
                                                <source src={this.state.videoFromInputUrl} type="video/mp4" />
                                            </video>
                                        </div>}
                                    <Message severity="info" text="Telegram upload max-size 50MB" style={{width: '100%',background: '#f0f0f0'}} />
                                </AccordionTab>

                                <AccordionTab header="Document">
                                    <div className="p-toolbar-group-left">
                                        <Button label="Add document file" icon="pi pi-plus" className="p-button-secondary"
                                            style={{marginBottom:'10px'}} onClick={(e) => document.getElementById('docUpload').click()} />
                                        <div style={{display: 'none'}}>
                                            <input id="docUpload" className="fileInput" type="file" 
                                                onChange={(e)=>this.handleDocChange(e)} />
                                        </div>
                                    </div>
                                    {this.state.docLoading &&
                                    <div style={{clear: 'both'}}>
                                    <ProgressBar mode="indeterminate" style={{height: '6px'}}></ProgressBar>
                                    </div>
                                    }
                                    <div className="p-toolbar-group-right">
                                        <Button icon="pi pi-times" className="p-button-secondary" 
                                        onClick={(e) => {this.setState({docFromUrl: ''});} } />
                                    </div>
                                    <div style={{clear: 'both'}}>
                                        <p>
                                            Or paste document from URL:
                                        </p>
                                    </div>
                                    <div style={{clear: 'both'}}>
                                        <InputTextarea rows={4} style={{width: '100%'}} placeholder="Paste document URL" 
                                        value={this.state.videoFrom} 
                                        onChange={(e) => {this.setState({videoFromUrl: e.target.value});}} />
                                    </div>
                                </AccordionTab>

                                <AccordionTab header="Inline buttons"></AccordionTab>
                                <AccordionTab header="Poll"></AccordionTab>
                            </Accordion>
                        </p>
                        <p>
                            <div>Select bot to send message with.</div>
                        </p>
                        <p>
                            <Dropdown options={this.props.telegramBotTokens} value={this.state.dropdownToken} 
                            onChange={event => this.setState({dropdownToken: event.value})} 
                            autoWidth={true} style={{width:'100%'}} />
                        </p>
                        <p>
                            <div>Select channel</div>
                        </p>
                        <p>
                            <Dropdown options={this.props.telegramChannels} value={this.state.dropdownChannel} 
                            onChange={event => this.setState({dropdownChannel: event.value})} 
                            autoWidth={true} style={{width:'100%'}} />
                        </p>
                        <p>
                            <Button icon="pi pi-check" label="Send message" 
                            style={{marginBottom:'10px', width:'100%'}} 
                            onClick={this.handleSendMessage} 
                            disabled={this.state.sendMsgDisabled} />
                        </p>
                        {this.state.sendMsgDisabled &&
                        <ProgressBar mode="indeterminate" style={{height: '6px'}}></ProgressBar>
                        }
                    </div>
                </div>
                <div className="p-col-12 p-lg-6">
                    <div className="card card-w-title">
                        <h1>Tokens, channels</h1>
                        <Growl ref={(el) => this.growl = el} />
                        <TabView>
                            <TabPanel header="Bot tokens">
                                <div className="p-col-12">
                                    <p>Token to access the Telegram bot HTTP API. This bot will handle 
                                    message send. You can get it from <i>@BotFather</i>.</p>
                                </div>
                                <div className="p-col-12">
                                    <Dropdown options={this.props.telegramBotTokens} value={this.state.dropdownDeleteToken} 
                                    onChange={this.selectDropdownToken} 
                                    autoWidth={true} style={{width:'100%', marginBottom: '17px'}} />
                                </div>
                                <div className="p-col-12">
                                    <div className="p-inputgroup">
                                        <span className="p-inputgroup-addon">@</span>
                                        <InputText placeholder="Bot name (@MyOwnBot, @PostBot ...)" 
                                        style={{width:'100%'}} value={this.state.inputBotName}
                                        onChange={event => this.setState({inputBotName: event.target.value})} />
                                    </div>
                                </div>
                                {this.state.inputBotNameError && 
                                    <div className="p-col-12">
                                    <Message severity="error" text="Field is required" 
                                    style={{width:'100%'}}></Message>
                                    </div>}
                                <div className="p-col-12">
                                    <InputText placeholder="Token" style={{width:'100%'}} 
                                    value={this.state.inputBotToken}
                                    onChange={event => this.setState({inputBotToken: event.target.value})} />
                                </div>
                                {this.state.inputBotTokenError && 
                                    <div className="p-col-12">
                                    <Message severity="error" text="Field is required" 
                                    style={{width:'100%'}}></Message>
                                    </div>}
                                <div className="p-col-12">
                                    <Button icon="pi pi-save" label="Save" 
                                    style={{marginBottom:'7px', width:'100%'}} 
                                    onClick={this.handleAddBotToken} />
                                    {(this.state.dropdownDeleteToken 
                                        && this.state.inputBotName !== '' 
                                        && this.state.inputBotToken !== '') && 
                                        <div>
                                            <Button label="Cancel" icon="pi"
                                            style={{marginBottom:'7px', width:'100%'}} 
                                            onClick={(e) => {this.setState({dropdownDeleteToken: null, inputBotName: '', inputBotToken: ''})}} 
                                            className="p-button-secondary" />
                                        </div>
                                    }
                                    {this.state.dropdownDeleteToken &&
                                        <div>
                                            {this.state.checkDeleteToken &&
                                            <Button icon="pi pi-times" label="Delete" 
                                            style={{width:'100%'}} 
                                            onClick={this.handleDeleteBotToken} 
                                            className="p-button-danger" 
                                            disabled={this.state.deleteButtonDisabled} />
                                            }
                                            <div style={{textAlign:'right', paddingTop: '7px'}}>
                                                <Checkbox inputId="cb1" onChange={this.handleCheckDeleteBotToken} 
                                                checked={this.state.checkDeleteToken} />
                                                <label htmlFor="cb1" className="p-checkbox-label">
                                                    Check to delete
                                                </label>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </TabPanel>

                            <TabPanel header="Channels">
                                <div className="p-col-12">
                                    <p>Add channel created from your telegram account. Also you need add bot to the channel and give post privileges.</p>
                                </div>
                                {this.props.telegramChannels.length > 0 &&
                                <div className="p-col-12">
                                    <Dropdown options={this.props.telegramChannels} value={this.state.dropdownDeleteChannel} 
                                    onChange={this.selectDropdownChannel} 
                                    autoWidth={true} style={{width:'100%', marginBottom: '17px'}} />
                                </div>}
                                <div className="p-col-12">
                                    <div className="p-inputgroup">
                                        <span className="p-inputgroup-addon">@</span>
                                        <InputText placeholder="Channel name or ID (@FakeNewsChannel, 11233885855)" 
                                        style={{width:'100%'}} value={this.state.inputChannelName}
                                        onChange={event => this.setState({inputChannelName: event.target.value})} />
                                    </div>
                                </div>
                                {this.state.inputChannelNameError && 
                                    <div className="p-col-12">
                                    <Message severity="error" text="Field is required" 
                                    style={{width:'100%'}}></Message>
                                    </div>}
                                <div className="p-col-12">
                                    <InputTextarea placeholder="Description (cats pics channel, naked truth ...)" style={{width:'100%'}} 
                                    value={this.state.inputChannelDesc}
                                    onChange={event => this.setState({inputChannelDesc: event.target.value})} />
                                </div>
                                {this.state.inputChannelDescError && 
                                    <div className="p-col-12">
                                    <Message severity="error" text="Field is required" 
                                    style={{width:'100%'}}></Message>
                                    </div>}
                                <div className="p-col-12">
                                    <Button icon="pi pi-check" label="Save" 
                                    style={{marginBottom:'10px', width:'100%'}} 
                                    onClick={this.handleAddChannel} />
                                    {(this.state.dropdownDeleteChannel 
                                        && this.state.inputChannelName !== '' 
                                        && this.state.inputChannelToken !== '') && 
                                        <div>
                                            <Button label="Cancel" icon="pi"
                                            style={{marginBottom:'7px', width:'100%'}} 
                                            onClick={(e) => {this.setState({dropdownDeleteChannel: null, inputChannelName: '', inputChannelDesc: ''})}} 
                                            className="p-button-secondary" />
                                        </div>
                                    }
                                    {this.state.dropdownDeleteChannel &&
                                    <div>
                                        {this.state.checkDeleteChannel &&
                                        <Button icon="pi pi-times" label="Delete" 
                                        style={{marginBottom:'10px', width:'100%'}} 
                                        onClick={this.handleDeleteChannel} 
                                        className="p-button-danger" 
                                        disabled={this.state.deleteButtonChannelDisabled} />
                                        }
                                        <div style={{paddingTop: '7px', textAlign: 'right'}}>
                                            <Checkbox inputId="cb1" onChange={this.handleCheckDeleteChannel} 
                                            checked={this.state.checkDeleteChannel} />
                                            <label htmlFor="cb1" className="p-checkbox-label">
                                                Check to confirm delete
                                            </label>
                                        </div>
                                    </div>
                                    }
                                </div>
                            </TabPanel>
                        </TabView>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TelegramComponent);